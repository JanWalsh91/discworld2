# 2021.08.21

Just enough magic to influence belief

Belief creates the outcome. Gods are powered by belief. Gods can come into existence through belief.

Lord Walden: current patrician of Morpork. Made the city the richest city on the disc, but with the poorest people through taxes, through tax farming (sell off the rights to tax things)

Riots are starting

No licenced thievery. No guilds have been formed.

Cable Street Particulars / the Unmentionnables
- essentially mafia
- we just got caught by them

We all lived in the same shitty bunk house. The CSP stormed in and we got sent to the palace prisons, instead of their prisons. We will be sent to the CSP.

Races:
- PHB
- Elemental Evil (aaroka, genassi, goliath)
- Volo's, no tritons, no yuan-ti. Shifter ok.


Character creation:
- Stat reallocation OK.
- Lvl 1 feat. Must take ASI at lvl 4. Start at lvl 3.
- Background: stick with one or two familiar NPC
- We start with no equipment

No guilds.

Gangs go around and collect taxes. 

aasimar - Wyrmberg: seen as magically irradiated, seen as walking bomb

tielfing - pact of the fiend - touched by something from the Dungeon Dimension

reptilian - Genua

dwarves - ramtops

orcs - nomadic

genassi - Klatch

birdlike - Uverwald

elves - Llamedos 


Wyrmburg - carved mountain with smooth sides, runic, flipped upsides down. Stands that way because of concentration of magic. Because I've lived there for so long, my species is changed. People are suspicious of the Wyrmburg. We are elf-like. Around: magically altered plains. Octarine colored planes. Close to being the vortex plan.
Similiar to human society, but less grimy. Things are cleaner and nicer. Not a perfect society, but good. They live on the outer side and the inside of the cone, going up to the top that also houses people.

Uncanny valley effect. People can tell that there's something off. Aasimar can pass off as elf. Other elves will know that I'm not elven. Looks too smooth, too good.