# Pyra Skyforge

## Overview

Forge Cleric, Aasmiar.

## Intro

"Check it out, Hemry, it's the third time I saw her this week."

Hemry's head swerved around. "That one 'ere with the curly black hair?" He took a loud sip of ale, attempting to focus his eyes on the chocolate colored figure who just entered the bar. "Ah ... she sure is a beaut', ain't she?"

"Is she?" Devlin set his pint down and narrowed his eyes. "Look, she's about take the same spot she did yesterday and talk to the barman again. Ok ... wait for it... There! See how the light hits her cheek? There's no way the sun on this stinking day can make a cheek like *that* that shine like gold. That's not natural skin." He carefully sips his ale. "I don't know, there's something strange about her. Her perfect hair, her perfect skin, her perfect pointed ears..."

"Well hey Dev, if you want to talk to 'er there's room at this table right 'ere."

Devlin grabs Hemry as his friend's hand darted for an unoccupied chair. "Let it go Hemry," he muttered aggressively under his breath. "I'm not interested in her in that that way." Hemry settled back onto the bench, clutching the armrest to stabilize himself. Assured that his friend won't make any unexpected decisions, Devlin returned to inspecting his subject." She's even sweating a bit on her brow. What do you think she's been up to all day?"

"*hic* Whadda you think she does with them meaty arms of hers, heh heh..."

*WHACK*

"Ow! Dev!"

"Dammit Hemry. Just shut the hell up."

The subject and the barman exchanged a few words as she downed her drink, after which he gave a thoughtful glance in the air and scribbled a quick note that he handed her with an almost-confident nod. Devlin caught her beaming smile and ingenuous eyes in the rows of bottles behind the barman as she took the note and made her way out. The barman gave her a quick wave. "Hey Pyra! I hope you find what you're looking for!"

## Aesthetics

- Shields, Spears, and Fire.
- Chocolate, Gold, and Red.

## Physical Description

- Subtle elvish features
- Tightly coiled voluminous silky black hair
- Milk chocolate skin with golden highlights
- Amber eyes
- 24 years old, female

## Personal Relationships

- Born: Pyra Skyforge
- Father: Adric Skyforge
  - Metalsmith, first to teach Pyra
- Mother: Izara Ashenkeeper
- Younger Sister: Amara Skyforge
  - Jewelcrafter
- Teacher: Rada Jalburn, Female Aasimar bladesmith. Dark grey/blue skin, pure white eyes, Black loosely dreaded hair (Shoulder length)
  - Kindly but firm when teaching / forging, not very talkative about anything that isn't weapon or armor smithing. 
  - Would use any mistake in lessons as an opportunity to tell a story about her fathers own skills at the forge, clearly proud of the family tradition.
  - Weapon and Armorsmith, Pyra's elven forge mentor
- Croft Alder, forge elder elf living in Wyrmberg
  - Now aged and weak but known to have been a great crafter in his own time.
  - Bald, missing the tip of an ear, patchy stubble from singe marks on his cheeks and face, alabaster skin.
  - Focus was mainly on intricate decorative pieces and "Fiddly bits"
  - He sent me to Ankh Morpork to talk to Kalabaan Hist, an elven smith in the city at "12 Brookless Way".
    - I need to fetch an artifact for Croft, something which Croft and Kalabaan have been working on together to learn about.
    - He sent me since he's too old and frail to fetch it himself
  - Croft knew Rada's father, but neither he or Rada talk about where he is or what happened to him, or even speak his name. They both somehow always manage to change smoothly the topic if questioned. 

## Forging Culture

Just like in most cultures, Aasimar have a lot of focuses, not just smithing, but IN the groups that truly smith like their hearts are in it they often worship in their rough little way the God Neolidan. Perhaps not through actual prayer and praise, but in actions and mutterings. Thanking his name for a good bit of steel or a well turned blade. Aasimar forging is straightforward, effective, hammer to metal and sweat on the brow.

Elvish crafting is a lot more graceful but inefficiently flamboyant, often involving more show than work. It still gets results at least, especially in their specialties of silverwork and decorative metals. Most elves in the craft don't worship Neolidan, but not out of spite or anything, they simply believe more in the craft of their own hands

## Personality Traits

- Nothing can shake my optimistic attitude
- Despite my arcane origins, I do not place myself above other folk. We all have the same blood.
- I face problems head.on. A simple, direct solution is the best path to success.

## Ideals

- Charity: I always try to help those in need, no matter what the personal cost. (Good)
- Change. Life is like the seasons, in constant change, and we must change with il. (Chaotic)
- No Limits: Nothing should fetter the infinite possibility inherent in ali existence. (Chaotic)

## Bonds

- I am determined to recover a lost relic of my faith that was lost long ago.
- My loyalty to my faith is unwavering.
- My family and my people are the most important thing in my life, even when they are far from me.

## Flaw

- I'd risk too much to uncover a bit of lost knowledge.
- Unlocking an ancient mystery is worth the price of a civilization.
- I can't keep a secret to save my life, or anyone else's.

## Backstory

Born to family of metalworkers in the heights of Wyrmberg, I, Pyra Skyforge and my sister Amara poured owr curiosity into tinkering with leftovers from our father's, Adric's, workshop. As we grew up, Adric taught us the tools of the trade, and before long, at 15 years old, I left to study abroad as an apprentice in Llamedos under the famed aasimar master Rada Jalburn. Rada loved talking about some old master of hers, and I eventually discovered that she was talking about her father. I also met Croft Alder, a forge master who regularly came to visit from Wyrmberg. He was old and bald but must have been a great crafter in his time. Though they mostly talked about their passion of smithing, on more than one occasion I overheard them talking about Rada's father. But when I tried to confront them, they always seemed to find a way to avoid the topic.

I didn't make much of it, as I, too, was passionate about the craft. Learning not only advanced smithing techniques but also elvish, I returned to Wyrmberg and helped my father build his business as I worked with him for several years. As is custom in our family and in my fellow smithing artisans, I grew to believe that Neoldian, god of the forge, inspires us all in our work. I even started feeling a connection with him and and draw power and inspiration from him.

Praised for my excellent craftsmanship, passionate about my work, and dying to learn more, I finally decided to leave my hometown in a lifelong quest to learn from the great masters of metal around the Disc, and explore the world searching for ancient weapons and secrets of the forge. Before I left, she received basic combat training with shield and spear, as is custom among the warriors of my people.

On the day of my departure, I was approached by Croft who asks to pick up an artifact in Ankh Morpork for him. How convenient that it's my first destination! He asked me seek out "Kalabaan Hist" an Elven smith. He merely gave me the address "12 Brookless Way".

I arrived in Ankh Morpork by boat, and after finding a simple place to stay, I quickly got to the task of finding Kalabaan. Unfortunately, the address Croft gave me was in the fancy part of town, and all the bridges are guarded and they ask for crazy tolls to get through, and so, I haven't found a way to get there yet.

I am close to my family, and especially my sister Amara, who married into a noble family a few years before Pyra left. We occasionally send each other letters. 

## Custom Background: Skilled Artisan

You are skilled in thaumaturgic metallurgy, and spent many years studying, researching, and practicing the craft. You and your associates, family, or guild have gained a reputation that has spread into the mercantile world and professional circles. You have learned your skills as an apprentice to a master artisan until you became a skilled in your own right and a life-long student. 

Skill Proficiencies: 
- Arcana
- History

Tool Proficiencies:
- One type of artisan's tools

Languages:
- One of your choice

Equipment:
- Set of traveler's clothes

Feature - Student of the Trade:
- Years of practice and research in metallurgy has given you insight into the identifying cultural origins of objects made of metal, as well as the methods and materials used to forge them.

## Long-term goal

Seek out great metal workers and learn from them, as well as search the lands for forgotten artifacts, hunting for clues as to where I might find them, and to see if any techniques have been forgotten and might be refound and for ancient weapons reforged.

## Medium-term goal

Find the artifact that [.....] sent her out to fetch and return it to Wyrmberg.

## Short-term goal

Seeking out a forge master in Ankh Morpork who has knowledge about the creating of items of metal tempered specifically to ready them for enchanting.

## Stats

Rolled: 18 14 13 13 13 11

Aasimar: +2 WIS, +1 CON

_ | STR | DEX | CON | INT | WIS | CHA | Max HP | Hit die
-|-|-|-|-|-|-|-|-
Array     | 14 | 13 | 13     | 13 | 18     | 11 |
Aasimar   | 14 | 13 | 14(+1) | 13 | 20 (+2)| 11 |
Cleric 1  | 14 | 13 | 14     | 13 | 20     | 11 | 10 (8+2)    | 1d8
Cleric 2  | 14 | 13 | 14     | 13 | 20     | 11 | 17 (10+5+2) | 2d8
Cleric 3  | 14 | 13 | 14     | 13 | 20     | 11 | 24 (17+5+2) | 3d8
Cleric 4  | 14 | 13 | 16     | 13 | 20     | 11 | 35 (24+5+3+3) | 4d8

## Detailed Progression

- Level 1
  - Race: Aasimar
    - +2 WIS, +1 CON
    - Darkvision:
      - Blessed with a radiant soul, your vision can easily cut through darkness. You can see in dim light within 60ft of you as if it were bright light, and in darkness as if it were dim light. You can't discern color in darkness, only shades of grey.
    - Celestial Resistance:
      - You have resistance to necrotic damage and radiant damage.
    - Healing Hands:
      - As an action, you can touch a creature and cause it to regain a number of hit points equal to your level. 1/long rest.
    - Light Bearer:
      - You know the light cantrip. Charisma is your spellcasting ability for it.
    - Languages: Common, Celestial
    - Radiant Soul:
      - Starting at 3rd level, you can use your action to unleash the divine energy within yourself, causing your eyes to glimmer and two luminous, incorporeal wings to sprout from your back. Your transformation lasts for 1 minute or until you end it as a bonus action. During it, you have a flying speed or 30 feet, and once of each of your turns, you can deal extra radiant damage to one target when you deal damage to it with an attack or a spell. The extra radiant damage equals your level. 1/long rest
  - Background: Skilled Artisan
    - Proficiencies:
      - Arcana
      - History
      - Tinker's tools
    - Languages: Elven
    - Feature: 
  - Cleric 1
    - Saving Throws: WIS, CHA
    - Proficiencies:
      - light armor
      - medium armor
      - shields
      - simple weapons
    - Skills: (2 from: History, Insight, Medicine, Persuasion, Religion)
      - Insight
      - Medicine
    - 3 cantrips
      - Word of Radiance
      - Sacred Flame
      - Toll the Dead
    - 2 1st level spell slots
    - Ritual Casting
    - Divine Domain:
      - Domain Spells:
        - Identify
        - Searing Smite
      - Proficiencies:
        - Heavy Armor
        - Smith's Tools 
      - Blessing of the Forge
        - You gain the ability to imbue magic into a weapon or armor. At the end of a long rest, you can touch one nonmagical object that is a suit of armor or a simple or martial weapon. Until the end of your next long rest or until you die, the object becomes a magic item, granting a +1 bonus or AC if it's armor, or a +1 bonus to attack and damage rolls if it's a weapon. 1/long rest.
  - Feat: War Caster
- Level 2
  - Cleric 2
    - Channel Divinity: 1/short rest
    - Turn Undead:
      - As as action, you present your holy symbol and speak a prayer censuring the undead. Each undead that can see or hear you within 30ft must make a WIS saving throw. If the creature fails its saving throw, it is turned for 1 minute or until it takes any damage. A turned creature must spend its turns trying to move away from you as it can, and it can't willingly move to a space within 30 feet from you. It also can't take reactions. For its action, it can use only the Dash action or try to escape from an effect that prevents it from moving. If there's nowhere to move, the creature can use the Dodge action.
  - Forge Cleric:
    - Artisan's Blessing:
      - You conduct and hour-long ritual that crafts a nonmagical item that must include some metal: a simple or martial weapon, a suit or armor, ten pieces of ammunition, a set of tools, or another object (see chapter 5 "Equipment" in the Player's Handbook for examples of those items). The creation is completed at the end of the hour, coalescing in an unoccupied space of your choice on a surface within 5ft of you. The thing you create can be something that is worth no more than 100gp. As part of this ritual, you must lay out metal which can include coins, with a value equal to the creation. The metal irretrievably coalesces and transforms into the creation at the ritual's end, magically forming even nonmetal parts of the creation. The ritual can create a duplicate of a nonmagical item that contains metal, such as a key, if you possess the original during the ritual.
- Level 3
  - Cleric 3
    - Level 2 spell slots
      - From Tasha's: Heat Metal, Magic Weapon
- Level 4
  - Cleric 4
    - +1 Cantrip known
      - Guidance
    - + 1 2nd level spell slot
    - ASI (+2 CON)

