# Cantrips

## Selected

- Word of Radiance
- Sacred Flame
- Toll the Dead

## Xanathar's

- Toll the Dead
- Word of Radiance

# Level 1

## Forge Domain Spells

- Identify
- Searing Smite

## Player's Handbook Cleric Spells

- Bane
- Bless
- Command
- Create or Destroy Water
- Cure Wounds
- Detect Evil and Good
- Detect Magic
- Detect Poison and Disease
- Guiding Bolt
- Healing Word
- Inflict Wounds
- Protection from Evil and Good
- Sanctuary
- Shield of Faith

## Xanathar's

- Ceremony

# Level 2

## Player's Handbook Cleric Spells

- Aid
- Augury
- Blindness/Deqfness
- Calm Emotions
- Continual Flame
- Enhance Ability
- Find Traps
- Gentle Repose
- Hold Person
- Lesser Restauration
- Locate Object
- Prayer of Healing
- Protection from Poison
- Silence
- Spiritual Weapon
- Warding Bond
- Zone of Truth

## Forge Domain Spells

- Heat Metal
- Magic Weapon

# Level 3

## Forge Domain Spells

- Elemental Weapon
- Protection from Energy

## Xanathar's

- Life Transference

# Level 4

## Forge Domain Spells

- Fabricate
- Wall of Fire

# Level 5

## Forge Domain Spells

- Animate Objects
- Creation

## Xanathar's

- Dawn
- Holy Weapon

# Level 6

# Level 7

## Xanathar's

- Temple of the Gods